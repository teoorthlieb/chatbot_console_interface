import requests
import random
import json


def display_ans(ans):
    pre = "Koios: "

    # Tab the ans so it looks good with the prefix
    ansplit = ans.split("\n")
    newans = ansplit[0] + "\n"
    for i in range(1, len(ansplit)):
        newans += len(pre)*" " + ansplit[i] + "\n"

    # Print the tabbed answer
    print(f"{pre}{newans}", end="")


def main():
    data = {}
    random.seed(a=None)
    data['user_id'] = str(random.random()*1000000)
    data['lang'] = "en"
    while True:
        # Send the user message
        datajson = json.dumps(data)
        try:
            response = requests.post('http://127.0.0.1:5000', data=datajson)
        except requests.exceptions.ConnectionError:
            print("Oops... it seems the chatbot is not online, you should start the chatbot and retry.")
            break

        try:
            res = json.loads(response.content)
        except json.JSONDecodeError:
            print("The chatbot gave a unusual answer, it's very likely that it encountered a bug that needs to be fixed!")
            break
        # print(">>>", res)

        # Display Koios's answer
        display_ans(res["message"])
        payload = res["payload"]
        for i, choice in enumerate(payload):
            print(str(i+1)+".", choice["message_sent"])

        # Stop if the dialogue has ended
        if res["state"] == "end":
            break

        # Allow the user to answer back
        msg = input("> ").strip()
        if (msg == "fr" or msg == "en") and msg != data['lang']:
            data['lang'] = msg
        else:
            if res["reply_type"] == "checkbox":
                try:
                    nums = msg.replace(",", " ").split(" ")
                    answer = []
                    for num in nums:
                        if num.strip().isdigit():
                            val = int(num)-1
                            if 0 <= val < len(payload):
                                answer.append(payload[val]["payload"])
                    data['payload'] = answer
                except (KeyError, IndexError):
                    data['payload'] = msg
            else:
                try:
                    num = int(msg)
                    data['payload'] = payload[num-1]["payload"]
                except (KeyError, IndexError):
                    data['payload'] = msg
            data['reply_type'] = res['reply_type']
            data['state'] = res['state']

        # Print a new line for readability
        print()


main()
